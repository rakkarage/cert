docker run -it --rm \
	-v /docker-volumes/etc/letsencrypt:/etc/letsencrypt \
	-v /docker-volumes/var/lib/letsencrypt:/var/lib/letsencrypt \
	-v /docker-volumes/var/log/letsencrypt:/var/log/letsencrypt \
	-v ~/Data/code/cert/cert-site:/data/letsencrypt \
	certbot/certbot \
	certonly --webroot \
	--register-unsafely-without-email --agree-tos \
	--webroot-path=/data/letsencrypt \
	--staging \
	-d henrysoftware.ca -d www.henrysoftware.ca -d chat.henrysoftware.ca
